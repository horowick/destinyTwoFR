# Armes éxotiques de Destiny 2

#### Salut tout le monde ! Lors d'un projet en Qt Quick ayant pour but d'utiliser les armes éxotiques du jeu de Bungie : *Destiny 2*, j'ai créé un fichier JSON avec la liste des armes contenant nom, type, classe, effet et même la miniature.

*Attention: Le nom des armes n'est peut-être pas équivalent à ceux du jeu, la traduction n'étant pas la même pour tout le monde*

## Exemple

```json
{
    "name": "L'Airelle",
    "class": "Mitraillette légère",
    "type": "cinétique",
    "effect" : "\"Chevaucher le taureau\": Augmente la vitesse de tir et le recul lorsque la gâchette est enfoncée. Tuer avec cette arme recharge une partie du chargeur.",
    "image": "imgs/huckleberry.jpg"
},
```


